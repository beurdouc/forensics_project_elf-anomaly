Romain Beurdouche

# Forensics, Project: `elf-anomaly`

This project is about writing a tool named `elf-anomaly` which takes the path of an ELF file and look for some anomalies in this file.
So, I wrote a Python script which uses the `lief` library to parse the ELF file.

The tool is implemented in the script `elf-anomaly.py`. The other python scripts are drafts.

Some functionalities described in the original project statement. I explain why in the "Improvements" section of this file.

## Run the tool

### Prerequisites

1. The script is a Python script running under Python 3.

2. This script uses the `lief` library.

Install with pip:
```
$ sudo pip3 install lief
```

lief repository: https://github.com/lief-project/LIEF
lief documentation: https://lief.quarkslab.com/doc/latest/index.html

### Synopsis:

```
$ python3 elf-anomaly.py <input_file_path> [<output_file_path>]
```

## Implemented Checks:

### First, Parsing the file

The `lief` library provide the function `lief.ELF.parse` which given a file path returns an object from the `binary` type which represent the parsed ELF file.

### Search for symbol information.

The `binary` object gives an array of all the entries in the symbol table.
If this array is None or of size 0, then the symbols information are considered missing.

### Check if the Section header do not go beyond the file data.

This is simply done by comparing the offset of the section header plus its size to the size of the file.
The information about the section header can be find in the `header` attribute of the `binary` object, which is itself an object.

### Check if there are no wrong entry points

The `header` attribute of `binary` has an attribute which hold the index of the section which contain the string table in the array of sections provided by `binary`.
We can then get the virtual address and the size of this section and check for every string index that we know if this index falls in this section.

### Check if there are any overlapping segments

The `binary` object has an attribute which is a list of object representing segments: `segments`.
Each of this objects gives the offset and the size of the corresponding segment.
From this we can determine the bounds of every segment.
Then they are sorted by offset so that we can easily see if they are overlapping by looking at the bounds of each consecutive segments.

I observed with this test that even in benign elf files, some segments may overlap.

### Check section's entropy

The `binary` object gives a list of the section with their name and their entropy.
The entropy of each section is then compared to a defined threshold.

### Check segment's permissions

The `segment` objects has an attribute named `flag` which states the permissions of each of this segments.
Given the signification of this flags (see: https://docs.oracle.com/cd/E19683-01/816-1386/6m7qcoblk/index.html#chapter6-tbl-39), we can interpret this flags and determine if the file has unusual permission.
The meaning of some of this flag is undefined, so the script verifies that all this flags are set to 0.
The script also verifies that a same segment has not the write and execute permissions.

### Check the entry point

The `lief` library provide a function to get the address of a function given its name.
It also allows to get the entry point of the file.
The script tests whether the entry point is equal to the address of the `_start` function or not.
If the `_start` function is not found, a special message is printed and the entry point is considered as abnormal.

__warning:__ I didn't manage to know if the file has a `_start` function or where the entry point address points at when the file is a __library__.
So the tool may signal that benign libraries have weird entry point.

### Check that the binary do not use an unusual loader

The `binary` objects has two attributes:
* `has_interpreter` which is a boolean equal to true if the binary uses a loader.
* `interpreter` which is a string equal to the path to the loader used by the binary.
So, if the binary uses an loader, we can check that this loader is known.
To do this, I used the `re` library which provide tools to test if a string matches a given regular expression (`"^/lib64/ld-linux-x86-64.so."`).

__warning:__ As you may have noticed by looking at the regular expression, the tool in its current version only recognizes binaries for linux based operating systems on x86_64 platforms.
The tool can be adapted to recognize binaries for other platforms by choosing the regular expression depending on the information given in the ELF header.

## Repair

If the tool is provided with its second file path as argument, it will try to fix the binary and write the resulting binary to the given path.
The `binary` objects of lief can be edited and then written as an ELF file using `Builder` class from lief.
Only a few fix are implemented in the current version of the tool.

### Segments permissions

A segment can have weird permissions. We can then edit this permissions.
If one segment has unknown permissions, which means that some flags of the segment header with no defined meaning are asserted high, we can deassert this flags using a bitwise and.
If one segment has write and execute permissions, the write flag is deasserted.

### Weird entry points

If a `_start` function is found but the entry point does not point to this function, then the entry point is set to the address of the `_start` function.

### interpreter

If the binary uses an interpreter but this interpreter is not recognized, the interpreter is set to a known value.

__warning:__ As explained above, the tool in its current version only recognizes the linux x86_64 default loader.
Trying to repair a binary designed for another operating system or another architecture will result in an erroneous file.

## Improvements

I didn't implement every possible checks and fix I could have implemented in this tool due to the limited time for the project.
In this section, I will explain how the tool can be improved and for some of them why I didn't already made this improvements.

### Further checks

One check asked in the project statement (see below section Reminder) was not implemented:

Detecting if `_start` does not call `__libc_start_main`: I was able to get the addresses of functions given their names using the `lief` library.
Unfortunately, I didn't find any way to tell if a function is called from another with `lief`.
Another idea I had but didn't had time to develop was to use the `objdump` command (or another) to get the disassembly of the code.
Shell commands can be run in a python script using the `os` module.

### Further fixes

The number of fixes available is small compared to the numbers of checks.

Some fixes are impossible to perform are completely irrelevant like fixing the entropy of a section as it will completely alter the content of the file.

Other errors and missing information in the headers are hard to fix since we have to recover the correct information from the body of the file.
This is mainly about missing symbol information, section headers going beyond file data or weird entry points when the `_start` function is not found.

Sometimes, fixing some problems requires taking decisions:

* For overlapping segments: How should we modify the size of sections? Is it even relevant to make this changes?
* For weird segment's permissions: If a segment has write and execute permissions, should we switch to write only or execute only?

Maybe we can add arguments to the tool so that the user can make this decisions.

### Handling more Operating Systems and Architectures

As explained above, the tool will not work properly on files for systems different from x86_64 Linux.
One major improvement of the tool would be to handle more operating systems and architectures or at least to disable the loader test when the target system is not handled.
At least extending the tool to x86 (32 bits) and ARM architectures can be an either useful and reasonable change.

## Reminder:

__Project Statement:__

[P7] Write an elf-anomaly tool that prints a list of checks to report the presence/absence of anomalies such as (no symbols info, se(ss|ct)ion header table pointing beyond file data, wrong string table index, overlapping headers/segments, unusual entropy of a section, strange segment permissions, weird entry point, different interpreter, _start that does not call __libc_start_main,…) The tool should also be able to (on demand) fix some of these issues (well, obviously the ones related to corrupted headers)

#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#this script is an attempt to test the presence of as much anomalies as possible in an ELF file
#the path to the file should be given as argument

import sys
import lief
import re

#------------------------------------------------------------------------------------------------------------

#parametrics
entropy_threshold=7#TODO: find a good threshold
interpreter_regex="^/lib64/ld-linux-x86-64.so."

#------------------------------------------------------------------------------------------------------------

#Read the arguments
if len(sys.argv)<=1:
	print("Usage: elf-anomaly <file_path>",file=sys.stderr)
	sys.exit(-1)
file_path = sys.argv[1]

#Parse the file
binary = lief.ELF.parse(file_path)

if binary is None:
	print("ERROR: Parsing failed",file=sys.stderr)
	sys.exit(-1)
	
#Get the header
header=binary.header
if header is None:
	print("ERROR: No Header found",file=sys.stderr)
	sys.exit(-1)

#------------------------------------------------------------------------------------------------------------

#Symbols Informations
print("symbols information are present:")
symbols_info_flag=True
if binary.symbols is None:
	print("ERROR: No symbol table",file=sys.stderr)
	symbols_info_flag=False
elif len(binary.symbols)==0:
	print("No symbol table")
	symbols_info_flag=False
#TODO something else to check?
if symbols_info_flag==True:
	print("OK")
else:
	print("NOK")
print("")

#------------------------------------------------------------------------------------------------------------
	
#Check that section headers do not go beyond file data
print("section headers table do not point beyond file data:")
section_header_flag=True
if header.section_header_offset is None:
	print("ERROR: Header Offset not available",file=sys.stderr)
	section_header_flag=False
elif header.section_header_size is None:
	print("ERROR: Header Entry size not available",file=sys.stderr)
	section_header_flag=False
elif binary.eof_offset is None:
	print("ERROR: End Of File offset not available",file=sys.stderr)
	section_header_flag=False
elif header.numberof_sections is None:
	print("no section header, skipping")
elif header.numberof_sections==0: #TODO is it the right test?
	print("no section header, skipping")
else:
	end_section_header=header.section_header_offset+(header.section_header_size*header.numberof_sections)
	if end_section_header>binary.eof_offset:
		section_header_flag=False
if section_header_flag==False:
	print("NOK")
else:
	print("OK")
print("")

#------------------------------------------------------------------------------------------------------------

#Check string table indexes
print("no wrong string table indexes:")
string_table_indexes_flag=True
if header.section_name_table_idx is None:
	print("ERROR: Sections information not available",file=sys.stderr)
	string_table_indexes_flag=False
elif binary.sections is None:
	print("ERROR: Sections information not available",file=sys.stderr)
	string_table_indexes_flag=True
else:
	if len(binary.sections)<=header.section_name_table_idx:
		print("Sections information not available")
		string_table_indexes_flag=False
	elif binary.sections[header.section_name_table_idx] is None:
		print("ERROR: Sections information not available",file=sys.stderr)
		string_table_indexes_flag=False
	elif binary.sections[header.section_name_table_idx].virtual_address is None:
		print("ERROR: Sections information not available",file=sys.stderr)
		string_table_indexes_flag=False
	elif binary.sections[header.section_name_table_idx].size is None:
		print("ERROR: Sections information not available",file=sys.stderr)
		string_table_indexes_flag=False
	else:
		min_idx=binary.sections[header.section_name_table_idx].virtual_address
		max_idx=min_idx+binary.sections[header.section_name_table_idx].size
		for section in binary.sections:
			if section.name_idx is None:
				print("ERROR: Sections information not available",file=sys.stderr)
				string_table_indexes_flag=False
			if section.offset is None:
				print("ERROR: Sections information not available",file=sys.stderr)
				string_table_indexes_flag=False
			else:
				if section.name_idx<min_idx or section.name_idx>max_idx:
					print("section with offset: "+section.offset+" has a wrong name index")
					string_table_indexes_flag=False
					
if string_table_indexes_flag==False:
	print("NOK")
else:
	print("OK")
print("")

#------------------------------------------------------------------------------------------------------------
	
#Check overlapping headers or segments
print("no overlapping headers/segments:")
overlap_flag=True
if header.header_size is None:
	print("ERROR: header size information not available",file=sys.stderr)
	overlap_flag=False
elif header.program_header_offset is None:
	print("ERROR: Program header offset information not available",file=sys.stderr)
	overlap_flag=False
elif header.program_header_size is None:
	print("ERROR: Program header size size information not available",file=sys.stderr)
	overlap_flag=False
elif header.numberof_segments is None:
	print("ERROR: Number of segments not available",file=sys.stderr)
	overlap_flag=False
elif header.section_header_offset is None:
	print("ERROR: Sections header offset not available",file=sys.stderr)
	overlap_flag=False
elif binary.segments is None:
	print("ERROR: Segments information not available",file=sys.stderr)
	overlap_flag=False
elif len(binary.segments)==0:
	print("Segments information not available")
	overlap_flag=False
else:
	if header.header_size>header.program_header_offset:
		print("ELF header and Program header overlap")
		overlap_flag=False
	end_program_header=header.program_header_offset+(header.program_header_size*header.numberof_segments)

	#Get segments locations
	nb_segments=len(binary.segments)
	segments_offsets=[0]*nb_segments
	segments_ends=[0]*nb_segments
	for i in range(0,nb_segments):
		if ((binary.segments[i].file_offset is None) or (binary.segments[i].file_offset is None) or (binary.segments[i].physical_size is None)):
			if overlap_flag==True:
				print("ERROR: Some segments information is missing",file=sys.stderr)
				overlap_flag=False
		else:
			segments_offsets[i]=binary.segments[i].file_offset
			segments_ends[i]=binary.segments[i].file_offset+binary.segments[i].physical_size
	#Sort the segments by offset
	nb_nul=0
	for i in range(0,nb_segments):
		j=i
		while (j>0 and segments_offsets[j]<segments_offsets[j-1]):
			segments_offsets[j],segments_offsets[j-1]=segments_offsets[j-1],segments_offsets[j]
			segments_ends[j],segments_ends[j-1]=segments_ends[j-1],segments_ends[j]
			j=j-1
		if segments_offsets[j]==0:
			nb_nul=nb_nul+1
	#the two next lines may be useful for debugging
	#print(segments_offsets)
	#print(segments_ends)
	#Some segment have an offset of 0 even in normal elf files.
	nb_no_nul=nb_segments-nb_nul
	no_nul_segments_offsets=[0]*nb_no_nul
	no_nul_segments_ends=[0]*nb_no_nul
	for i in range(0,nb_no_nul):
		no_nul_segments_offsets[i]=segments_offsets[nb_nul+i]
		no_nul_segments_ends[i]=segments_ends[nb_nul+i]

	if nb_no_nul>0 and header.program_header_size>0:
		#Warning: the Program header is a segment
		if end_program_header>no_nul_segments_offsets[0] and no_nul_segments_offsets[0]!=header.program_header_offset:
			print("Program header and first segment are overlapping")
			overlap_flag=False
		for i in range(0,nb_no_nul-1):
			if no_nul_segments_ends[i]>no_nul_segments_offsets[i+1] and no_nul_segments_offsets[i]!=no_nul_segments_offsets[i+1]:
				print("segment at offset "+str(no_nul_segments_offsets[i])+" overlapps with segment at offset "+str(no_nul_segments_offsets[i+1])+" from "+str(no_nul_segments_offsets[i+1])+" to "+str(no_nul_segments_ends[i]))
				overlap_flag=False
	else:
		print("No segments information available")
		overlap_flag=False
	
	if header.section_header_offset==0:
		print("no section header")
	else:
		if no_nul_segments_ends[nb_no_nul-1]>header.section_header_offset:
			print("last segment and Section header are overlapping")
			overlap_flag=False
			
if overlap_flag==False:
	print("NOK")
	print("Info: Overlapping segements may exist even in benign executables.")
	print("Have a look at the detail of overlapping segments above.")
else:
	print("OK")
print("")

#------------------------------------------------------------------------------------------------------------

#Check sections entropy
print("no sections with unusual entropy:")
section_entropy_flag=True
if binary.sections is None:
	print("ERROR: Sections information not available",file=sys.stderr)
	section_entropy_flag=True
else:
	for section in binary.sections:
		if section.entropy is None:
			if section_entropy_flag==True:
				print("Some sections entropy are missing",file=sys.stderr)
				section_entropy_flag=False
		elif section.name is None:
			if section_entropy_flag==True:
				print("Some sections name are missing",file=sys.stderr)
				section_entropy_flag=False
		else:
			if section.entropy>entropy_threshold:
				section_entropy_flag=False
				print(section.name+": "+str(section.entropy))
		
if section_entropy_flag==False:
	print("NOK")
else:
	print("OK")
print("")

#------------------------------------------------------------------------------------------------------------

#Check segments permissions
#see: https://docs.oracle.com/cd/E19683-01/816-1386/6m7qcoblk/index.html#chapter6-tbl-39
print("no segment with unusual permissions:")
segment_permission_flag=True
if binary.segments is None:
	print("ERROR: Segments information not available",file=sys.stderr)
	overlap_flag=False
else:
	for segment in binary.segments:
		#check that flags with undefined meaning are all set to 0
		if ((segment.flags is None) or (segment.file_offset is None)):
			if segment_permission_flag==True:
				print("ERROR: Some segments information is missing",file=sys.stderr)
				segment_permission_flag=False
		else:
			if segment.flags & 0x0ffffff8 != 0x0:
				print("segment at offset "+str(segment.file_offset)+" has unknown permission flags.")
				segment_permission_flag=False
			#check that the segment has not both write and execute permissions
			if segment.flags & 0x3 == 0x3:
				print("segment at offset "+str(segment.file_offset)+" has permission write and execute.")
				segment_permission_flag=False
			
if segment_permission_flag==False:
	print("NOK")
else:
	print("OK")
print("")

#------------------------------------------------------------------------------------------------------------
	
#Check weird entry point
print("no unusual entry point:")
entry_point_flag=True
if binary.entrypoint is None:
	print("ERROR: entrypoint missing",file=sys.stderr)
	entry_point_flag=False
else:
	try:
		_start_address=lief.ELF.Binary.get_function_address(binary,"_start")
	except lief.not_found:
		_start_address=-1
	if _start_address is None:
		print("_start function not found")
		entry_point_flag=False
	elif _start_address<=0:
		print("_start function not found")
		entry_point_flag=False
	elif _start_address!=binary.entrypoint:
		print("entry point is not _start ("+str(binary.entrypoint)+"!="+str(lief.ELF.get_function_address(binary,"_start"))+")")
		entry_point_flag=False
		
if entry_point_flag==False:
	print("NOK")
else:
	print("OK")
print("")

#------------------------------------------------------------------------------------------------------------

#Check interpreter
print("no unusual interpreter:")
interpreter_flag=True
if binary.has_interpreter is None:
	print("ERROR: Interpreter information not available",file=sys.stderr)
	interpreter_flag=False
elif binary.has_interpreter:
	if binary.interpreter is None:
		print("ERROR: Interpreter information not available",file=sys.stderr)
		interpreter_flag=False
	else:
		pattern=re.compile(interpreter_regex)
		if pattern.search(binary.interpreter)==False:
			print("interpreter "+binary.interpreter+" is an unusual interpreter")
			interpreter_flag=False
else:
	print("the binary does not use a loader")
if interpreter_flag==False:
	print("NOK")
else:
	print("OK")
print("")
	
